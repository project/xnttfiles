External Entities Files plugin
******************************

This module provides an external entity client storage plugin to manage files.
File external entities are managed through their file names as default entity
identifier and their file properties.

===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module provides an external entity client storage plugin to manage files.
File external entities are managed through their file names as default entity
identifier and their file properties.

A file external entity have the following fields (with values depending on the
operating system and the file system):
  - id        : entity identifier (usually the file path but it can be
                overriden through file name pattern in combination with a file
                name index file);
  - path      : full path to the file including file name;
  - dirname   : path to the file (excluding file name and trailing slash);
  - basename  : file name (including extension);
  - filename  : file name (excluding extension);
  - extension : file extension;
  - dev       : ID of device containing file;
  - ino       : inode number;
  - mode      : protection;
  - nlink     : number of hard links;
  - uid       : user ID of owner;
  - gid       : group ID of owner;
  - rdev      : device ID (if special file);
  - size      : total size, in bytes;
  - blksize   : blocksize for file system I/O;
  - blocks    : number of 512B blocks allocated;
  - atime     : time of last access (timestamp);
  - mtime     : time of last modification (timestamp);
  - ctime     : time of last status change (timestamp);
  - md5       : MD5 checksum if option is set in the entity type settings;
  - sha1      : SHA1 checksum if option is set in the entity type settings.

Additional field values can be extracted from the file path if a complex file
name pattern is used. For instance, the file name pattern "{group}/{name}.txt"
will add 2 fields to the file entity: "group", and "name". A matching file
"first_batch/test_results1.txt" will have the fields "group" = "first_batch" and
"name" = "test_results1".

It is possible to use more complex file name patterns to use sub-string of field
values in file name. It is also possible to override the entity identifier but
then, a file index file must be provided in order to associate an entity
identifier to a given file path. The file index file format is quite simple:
each line correspond to an entity; each line is composed by an entity
identifier, followed by a tab character, followed by the corresponding file path
(relative to the given directory in the entity type settings).

By default, files are stored in the Drupal's site "files" directory through the
file scheme "public://" but it is possible to specify other file scheme in the
entity file directory.

It is also possible to filter files processed by the plugin to restrict visible
external file entities. It can be achieved by a file name filter pattern in the
external entity type settings. It is based on regular expressions. It can also
be achieved using the file index file and restrict to files listed there.

The external entities files module also provide a base class to manage entities
stored in one or more files for other file plugin implementations.

The base class will handle file plugin settings and file structure management (
file-entity associations). A new file plugin would just need to implement file
content parsing and generation from raw entities.

To implement a new external entity file client (plugin), create a new class and
use Drupal\xnttfiles\Plugin\ExternalEntities\StorageClient\FileClientBase class
as a base class.
Ex.: `class TsvFiles extends FileClientBase { ... }`

Then, initialize some member variables in the constructor:
- fileType: human readable name of the file type. Ex.: 'yaml'.
- fileTypePlural: human readable plural name of the file type. Ex.: 'yamls'.
- fileTypeCap: human readable name of the file type with capitalized letters.
  For instance 'Yaml'.
- fileTypeCapPlural: human readable plural name of the file type with
  capitalized letters. For instance 'Yamls'.
- fileExtension: file extension including the dot (or it can be used just as a
  file suffix). Ex.: '.yml'

Finally, implement at least parseFile() and generateRawData() methods and
override what you need. A good example to follow is TsvFiles.php of the TSV
files external entities plugin (xntttsv).

REQUIREMENTS
------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will just provide classes for other file plugin
implementations.

TODO: add description of provided form settings (as describbed in xntttsv).

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
