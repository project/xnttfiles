<?php

namespace Drupal\xnttfiles\Plugin\ExternalEntities\StorageClient;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\external_entities\ExternalEntityInterface;
use Drupal\external_entities\ResponseDecoder\ResponseDecoderFactoryInterface;

/**
 * External entities storage client for files.
 *
 * @ExternalEntityStorageClient(
 *   id = "xnttfiles",
 *   label = @Translation("Files"),
 *   description = @Translation("Use files as entities.")
 * )
 */
class Files extends FileClientBase {

  /**
   * Constants used for entity save mode.
   */
  const SAVE_NOTHING = 0;
  const SAVE_FILE_MODE_DATE = 1;
  const SAVE_FILE_NAME = 2;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    ResponseDecoderFactoryInterface $response_decoder_factory,
    ConfigFactory $config_factory,
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    CacheBackendInterface $cache
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $response_decoder_factory,
      $config_factory,
      $messenger,
      $logger,
      $cache
    );
    // Defaults.
    $this->fileType = 'external';
    $this->fileTypePlural = 'external';
    $this->fileTypeCap = 'External';
    $this->fileTypeCapPlural = 'External';
    $this->fileExtension = '';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration()
    + [
      'md5_checksum' => NULL,
      'sha1_checksum' => NULL,
      'save_mode' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Customize form.
    $form['structure']['#title'] = $this->t(
      '@file_type file name pattern',
      ['@file_type' => $this->fileTypeCap]
    );
    $form['structure']['#description'] = $this->t(
      'Define a file and/or sub-directory name pattern using entity fields. If
       other fields than the identifier are used in the directory pattern, an
       index will be created to associate identifiers to their corresponding
       file path for efficiency.
       The pattern format uses curly braces to delimit entity fields to
       use and they can be prefixed by an offset and a length the following
       way: "{offset[,length]:field_name}". Negative values can be used the
       same way as for the PHP function substr().
       Ex: "{-4:id}/{0,3:id}.txt"
       with the identifier "12345678_ABCD" will generate the sub-path
       "ABCD/123.txt".
       If an empty value is provided, a default file name will be generated
       and used.'
    );
    $form['structure']['#required'] = FALSE;

    // Force single record.
    $form['multi_records']['#type'] = 'hidden';
    $form['multi_records']['#default_value'] = FALSE;

    // Hide field list since it won't be used.
    $form['field_list']['#type'] = 'hidden';

    // Add checksum options.
    $form['md5_checksum'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Compute MD5 checksum.'),
      '#description' => $this->t(
        'If checked, an MD5 checksum will be computed each time the entity is loaded. Be aware it can slowdown the system for large files.'
      ),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['md5_checksum'] ?? FALSE,
    ];
    $form['sha1_checksum'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Compute SHA1 checksum.'),
      '#description' => $this->t(
        'If checked, a SHA1 checksum will be computed each time the entity is loaded. Be aware it can slowdown the system for large files.'
      ),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['sha1_checksum'] ?? FALSE,
    ];

    // Add save option radio.
    $form['save_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Entity save behavior:'),
      '#options' => [
        static::SAVE_NOTHING => $this->t('Do nothing'),
        static::SAVE_FILE_MODE_DATE => $this->t('Update file last modification date (create empty file if missing)'),
        static::SAVE_FILE_NAME => $this->t('Rename file according to file name pattern (if needed)'),
      ],
      '#default_value' => $this->configuration['save_mode'] ?? static::SAVE_NOTHING,
    ];

    // Add save option radio.
    $form['available_fields'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<h4>Available fields are:</h4>
        <dl>
          <dt>id</dt>        <dd>entity identifier</dd>
          <dt>path</dt>      <dd>full path to the file including file name</dd>
          <dt>dirname</dt>   <dd>path to the file (excluding file name and trailing slash)</dd>
          <dt>basename</dt>  <dd>file name (including extension)</dd>
          <dt>filename</dt>  <dd>file name (excluding extension)</dd>
          <dt>extension</dt> <dd>file extension</dd>
          <dt>dev</dt>       <dd>ID of device containing file</dd>
          <dt>ino</dt>       <dd>inode number</dd>
          <dt>mode</dt>      <dd>protection</dd>
          <dt>nlink</dt>     <dd>number of hard links</dd>
          <dt>uid</dt>       <dd>user ID of owner</dd>
          <dt>gid</dt>       <dd>group ID of owner</dd>
          <dt>rdev</dt>      <dd>device ID (if special file)</dd>
          <dt>size</dt>      <dd>total size, in bytes</dd>
          <dt>blksize</dt>   <dd>blocksize for file system I/O</dd>
          <dt>blocks</dt>    <dd>number of 512B blocks allocated</dd>
          <dt>atime</dt>     <dd>time of last access (timestamp)</dd>
          <dt>mtime</dt>     <dd>time of last modification (timestamp)</dd>
          <dt>ctime</dt>     <dd>time of last status change (timestamp)</dd>
          <dt>md5</dt>       <dd>MD5 checksum if option is set</dd>
          <dt>sha1</dt>      <dd>SHA1 checksum if option is set</dd>
          <dt>*</dt>         <dd>other fields as specified by the user file name pattern setting</dd>
        </dl>
      '),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * Note: there is no ::submitConfigurationForm because of the PluginForm
   * system. All is done here.
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {

    // Validate file pattern.
    $structure = $form_state->getValue('structure');
    $structure = rtrim(ltrim($structure, static::PATH_TRIM), static::PATH_TRIM);
    if (empty($structure)) {
      $structure = '{id}';
      $form_state->setValue('structure', $structure);
    }

    $form_state->setValue('multi_records', FALSE);

    // Field list.
    $form_state->setValue('field_list', '');

    // Check checksums.
    $md5_checksum = $form_state->getValue('md5_checksum');
    $form_state->setValue('md5_checksum', $md5_checksum);
    $sha1_checksum = $form_state->getValue('sha1_checksum');
    $form_state->setValue('md5_checksum', $sha1_checksum);

    // Check save mode.
    $save_mode = $form_state->getValue('save_mode');
    $form_state->setValue('save_mode', $save_mode);

    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(ExternalEntityInterface $entity) {
    $entity_data = $entity->toRawData();
    $id = $entity_data[$this->getIdField()];
    // Get entity path.
    $entity_file_path = $this->getEntityFilePath($entity_data);
    switch ($this->configuration['save_mode']) {
      case static::SAVE_FILE_MODE_DATE:
        touch($entity_file_path);
        $result = SAVED_UPDATED;
        break;

      case static::SAVE_FILE_NAME:
        // Check if name changed.
        if (empty($entity_data['_xnttfiles_path'])) {
          // New file.
          touch($entity_file_path);
          $result = SAVED_NEW;
        }
        elseif ($entity_file_path != $entity_data['_xnttfiles_path']) {
          // Manage renaming.
          // Check that target file does not exist.
          if (file_exists($entity_file_path)) {
            // It exists, leave both original files and warn user.
            $this->messenger->addWarning(
              $this->t(
                'The new file name is already in use by another existing file. Could not rename entity file for entity "@entity".',
                [
                  '@entity' => $id,
                ]
              )
            );
            $this->logger->warning(
              'The new file name "'
              . $entity_file_path
              . '" is already in use by another existing file. Could not rename entity file "'
              . $entity_data['_xnttfiles_path']
              . '" for entity "'
              . $id
              . '".'
            );
          }
          else {
            // Check for missing target directories and create them.
            $directory = dirname($entity_file_path);
            if (!file_exists($directory)) {
              if (FALSE === mkdir($directory, 0777, TRUE)) {
                $this->messenger->addWarning(
                  $this->t(
                    'Unable to create the new target directory for entity "@entity".',
                    [
                      '@entity' => $id,
                    ]
                  )
                );
                $this->logger->warning(
                  'Unable to create the new target directory "'
                  . $directory
                  . '" for entity "'
                  . $id
                  . '".'
                );
              }
              else {
                // Rename/move file.
                if (!rename($entity_data['_xnttfiles_path'], $entity_file_path)
                ) {
                  $this->messenger->addWarning(
                    $this->t(
                      'Unable to rename/move file for entity "@entity".',
                      [
                        '@entity' => $id,
                      ]
                    )
                  );
                  $this->logger->warning(
                    'Unable to rename/move file to "'
                    . $entity_file_path
                    . '" for entity "'
                    . $id
                    . '".'
                  );
                }
              }
            }
          }
          $result = SAVED_UPDATED;
        }
        break;

      case static::SAVE_NOTHING:
      default:
        $result = SAVED_UPDATED;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * This method can be overriden and called by a child class in order to
   * aggregate more file info; for instance EXIF and other image info for JPEG
   * files, MP3 informations, video file format, length and resolution, etc.
   */
  protected function parseFile(string $file_path) :array {
    $data = [];
    // Get file "identifier".
    $path = $this->configuration['root'];
    $structure = $this->configuration['structure'];
    if (FALSE !== strpos($structure, '{')) {
      // Extract field values from file path.
      if ($entity = $this->getValuesFromPath($file_path)) {
        // Set a default id using file path if missing.
        $id = $entity[$this->getIdField()];
        $data[$id] = $entity;
      }
    }
    else {
      $id = $this->filePathToId(
        preg_replace('#^\Q' . $path . '\E/?#', '', $file_path)
      );
      $data[$id] = [$this->getIdField() => $id];
    }

    // Save current file name.
    $data[$id]['_xnttfiles_path'] = $file_path;

    // Open file.
    $fh = fopen($file_path, "r");

    // Gather statistics (remove non-string keys).
    $data[$id] += array_filter(
      fstat($fh),
      function ($k) {
        return !is_int($k);
      },
      ARRAY_FILTER_USE_KEY
    );

    // Close the file.
    fclose($fh);

    // @todo maybe convert uid to names with corresponding settings?
    // Might be useless since files are created by the web server but might be
    // usefull when using private directories outside Drupal's path.
    // See posix_getpwuid().
    // Get other file info.
    $pathinfo = pathinfo($file_path);

    // Adds full file path.
    if (empty($data[$id]['path'])) {
      $data[$id]['path'] = $file_path;
    }

    // Adds file directory.
    if (empty($data[$id]['dirname'])) {
      $data[$id]['dirname'] = $pathinfo['dirname'];
    }

    // Adds full filename.
    if (empty($data[$id]['basename'])) {
      $data[$id]['basename'] = $pathinfo['basename'];
    }

    // Adds extension if not in pattern.
    if (empty($data[$id]['extension'])) {
      $data[$id]['extension'] = $pathinfo['extension'];
    }

    // Adds filename without extension.
    if (empty($data[$id]['filename'])) {
      $data[$id]['filename'] = $pathinfo['basename'];
    }

    // Adds public URL.
    if (empty($data[$id]['public_url'])) {
      if ($wrapper = \Drupal::service('stream_wrapper_manager')->getViaUri($file_path)) {
        $data[$id]['public_url'] = $wrapper->getExternalUrl();
      }
    }

    // Adds checksum.
    if ($this->configuration['md5_checksum']) {
      $data[$id]['md5'] = md5_file($file_path);
    }
    if ($this->configuration['sha1_checksum']) {
      $data[$id]['sha1'] = sha1_file($file_path);
    }

    // Get columns.
    $data[''] = array_keys($data[$id]);

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  protected function generateRawData(array $entities_data) :string {
    return '';
  }

}
