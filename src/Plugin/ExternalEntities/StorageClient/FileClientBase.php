<?php

namespace Drupal\xnttfiles\Plugin\ExternalEntities\StorageClient;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\external_entities\ExternalEntityInterface;
use Drupal\external_entities\ExternalEntityTypeInterface;
use Drupal\external_entities\Plugin\PluginFormTrait;
use Drupal\external_entities\ResponseDecoder\ResponseDecoderFactoryInterface;
use Drupal\external_entities\StorageClient\ExternalEntityStorageClientBase;
use Drupal\xnttfiles\Exception\FilesExternalEntityException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract class for external entities storage client using files.
 */
abstract class FileClientBase extends ExternalEntityStorageClientBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * String used to trim path.
   */
  const PATH_TRIM = " \n\r\t\v\0\\/";

  /**
   * Regex to match substring parameters.
   */
  const SUBSTR_REGEXP = '(?:(-?\d+)(?:,(-?\d+))?:)?';

  /**
   * Regex to match field regex.
   */
  const FIELDRE_REGEXP = '(?:#([^#]*)#)?';

  /**
   * File type name (human readable).
   *
   * @var string
   */
  protected $fileType;

  /**
   * Plural file type name (human readable).
   *
   * @var string
   */
  protected $fileTypePlural;

  /**
   * Capitalized file type name (human readable).
   *
   * @var string
   */
  protected $fileTypeCap;

  /**
   * Plural capitalized file type name (human readable).
   *
   * @var string
   */
  protected $fileTypeCapPlural;

  /**
   * File type extension (including the dot).
   *
   * @var string
   */
  protected $fileExtension;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Loaded file data (cache).
   *
   * @var array
   */
  protected $fileEntityData;

  /**
   * Loaded file index (cache).
   *
   * @var array
   */
  protected $entityFileIndex;

  /**
   * Tells if an index file is needed.
   *
   * @var bool
   */
  protected $useIndexFile;

  /**
   * File name filtering regular expression.
   *
   * @var string
   */
  protected $fileFilterRegex;

  /**
   * Constructs a files external storage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\external_entities\ResponseDecoder\ResponseDecoderFactoryInterface $response_decoder_factory
   *   The response decoder factory service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger service (xnttfiles).
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    ResponseDecoderFactoryInterface $response_decoder_factory,
    ConfigFactory $config_factory,
    MessengerInterface $messenger,
    LoggerChannelInterface $logger,
    CacheBackendInterface $cache
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $response_decoder_factory
    );

    // Services injection.
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->cache = $cache;

    // Defaults.
    $this->fileType = 'external entity';
    $this->fileTypePlural = 'external entities';
    $this->fileTypeCap = 'External Entity';
    $this->fileTypeCapPlural = 'External Entities';
    $this->fileExtension = '.xntt';
    $this->fileEntityData = [];

    $no_id_structure = $this->configuration['structure'] ?? '';
    $no_id_structure = preg_replace(
      '~\{' . static::SUBSTR_REGEXP . '\Q' . $this->getIdField() . '\E\}~',
      '',
      $no_id_structure
    );
    $this->useIndexFile =
      !empty($this->configuration['index_file'])
      && str_contains($no_id_structure, '{');
    // Init file filter.
    $this->fileFilterRegex = $this->generateFileFilterRegex();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('external_entities.response_decoder_factory'),
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('logger.channel.xnttfiles'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'root'           => NULL,
      'structure'      => NULL,
      'matching_only'  => NULL,
      'multi_records'  => NULL,
      'field_list'     => NULL,
      'use_index'      => NULL,
      'index_file'     => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $value = '';
    $form['root'] = [
      '#type' => 'textfield',
      '#title' => $this->t(
        'Directory where the @file_type files are stored',
        ['@file_type' => $this->fileType]
      ),
      '#description' => $this->t(
        'Path to the main directory where @file_type files are stored (or sub-directories containing files when a pattern that includes sub-directories is used). Note: absolute/relative path are ignored and mapped to "@scheme://" file scheme (ie. Drupal site\'s "files" directory or its default file scheme path).',
        [
          '@file_type' => $this->fileType,
          '@scheme' => $this->configFactory->get('system.file')->get('default_scheme'),
        ]
      ),
      '#required' => TRUE,
      '#default_value' => $this->configuration['root'],
    ];

    $form['structure'] = [
      '#type' => 'textfield',
      '#title' => $this->t(
        '@file_type file name or file name pattern',
        ['@file_type' => $this->fileTypeCap]
      ),
      '#description' => $this->t(
        'This field can be set to a single @file_type file name if you store all
        your external entities into that file. Otherwise, you can define a file
        and/or sub-directory name pattern using external entity fields as
        placeholders. The pattern format uses curly braces to delimit entity
        fields to use. For instance the pattern "{group}/{project}.txt" would
        mean that the external entity provides values for its fields "group" and
        "project" and they can be used in the file path to the corresponding
        entity or entities if several entities (with same "group" and "project")
        can be stored in a same file. Note that only field names containing
        a-z, A-Z, 0-9 and "_" characters are considered valid.
        It is possible to use substrings of fields by specifying an offset and a
        length the following way:
        "{offset[,length]:field_name}". Negative values can be used the same way
        as for the PHP function substr().
        For example, the pattern "{-4:id}/{0,3:id}@file_ext" with the entity
        field id value "12345678_ABCD" will generate the sub-path
        "ABCD/123@file_ext" for that entity.
        If other fields than the entity identifier are used in the directory
        pattern, an index will be used to associate the identifiers to the
        corresponding file paths since the identifier can be the only available
        value to compute the corresponding entity file path. In such a case, a
        "entity-file index" file should be specified below.
        The index file can be pre-filled but it can also be generated here. In
        order to discover or restrict available files (and path), it is
        necessary to specify regular expressions to filter file names based on
        fields. Each field placeholder should contain, after the field name, a
        regex delimited by hash characters that will be used to filter valid
        values. For instance, in the first example, if we want to make sure we
        only match group names that start with a capital letter followed by 4
        numbers, we would use:
        "{group#[A-Z]\d{4}#}/{project}.txt". Then, when a the index file is
        generated or updated, it will only consider group directories matching
        the above pattern. However, since no pattern was specified for the
        "project" field, anything with a-z, A-Z, 0-9, "_" or "-" characters
        could be matched (which excludes by default subdirectories as they use
        slashes and dots).
        Note: if this field is left empty, a default free file name will be
        generated and used to store external entities.',
        [
          '@file_type' => $this->fileType,
          '@file_ext' => $this->fileExtension,
        ]
      ),
      '#required' => TRUE,
      '#default_value' => $this->configuration['structure'],
    ];

    // Restrict to files matching file name pattern.
    $form['matching_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enforce file name pattern.'),
      '#description' => $this->t(
        'Only allow files matching the file name pattern or present in the index file.'
      ),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['matching_only'] ?? TRUE,
    ];

    // Multiple records.
    $form['multi_records'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('A single file holds multiple entity records.'),
      '#description' => $this->t(
        'Check this box if a file can contain more than one entity.'
      ),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['multi_records'] ?? TRUE,
    ];

    // Index file.
    $form['index_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Index file'),
      '#description' => $this->t(
        'Path to the file used to index entities and their corresponding
        @file_type files. You can leave this field empty unless you use a file
        name pattern involving other fields than the entity identifier, or if
        you want to map a specific set of entities to other files than the
        default one (provided by the file name or name pattern). This file may
        be updated by this plugin when a non-indexed entity is saved and needs
        to be indexed (advanced file patterns). Already indexed entities (ie.
        the ones already present in this index file) will remain unchanged.',
        ['@file_type' => $this->fileType]
      ),
      '#default_value' => $this->configuration['index_file'],
    ];

    $form['use_index'] = [
      '#type' => 'checkbox',
      '#title' => $this->t(
        'Only use index file to list files (no directory listing).'
      ),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['use_index'] ?? FALSE,
    ];

    // Regenerate index button.
    $form['generate_index'] = [
      '#type' => 'submit',
      '#value' => $this->t('Regenerate entity-file index file'),
      '#description' => $this->t(
        'Clear previous entries (even manual) and regenerate a complete index file by listing and parsing valid data files.'
      ),
      '#name' => 'regen_' . $this->getPluginId(),
      '#submit' => [[
        $this,
        'regenerateIndexFileSubmit',
      ],
      ],
    ];

    // Regenerate index button.
    $form['update_index'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update entity-file index file'),
      '#description' => $this->t(
        'Adds missing entries to the index file, remove entries with unexisting files and leave other entries unchanged.'
      ),
      '#name' => 'upd_' . $this->getPluginId(),
      '#submit' => [[
        $this,
        'updateIndexFileSubmit',
      ],
      ],
    ];

    // User field list.
    $form['field_list'] = [
      '#type' => 'textfield',
      '#title' => $this->t(
        'List of entity field names to store in @file_type files',
        ['@file_type' => $this->fileType]
      ),
      '#description' => $this->t(
        'Leave this field empty if you want to save all the entity fields in new
        @file_type files. Otherwise, you can limit the fields saved into new
        @file_type files as well as set their order using this field. Just
        specify the field names separated by spaces, comas or semi-columns.',
        ['@file_type' => $this->fileType]
      ),
      '#default_value' => $this->configuration['field_list'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * Note: there is no ::submitConfigurationForm because of the PluginForm
   * system. All is done here.
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $default_scheme = $this
      ->configFactory
      ->get('system.file')
      ->get('default_scheme')
      . '://';

    // Check root directory.
    $root = $form_state->getValue('root');
    $root = rtrim(ltrim($root, static::PATH_TRIM), static::PATH_TRIM);
    // Adds ending slashes after scheme if removed or missing.
    $root = preg_replace('#:$#', '://', $root);
    // Check file scheme.
    if (0 === preg_match('#^\w+://#', $root)) {
      $root = $default_scheme . $root;
    }
    if (!file_exists($root) || !is_dir($root)) {
      $form_state->setErrorByName(
        'root',
        $this->t('Directory not accessible "@root".', ['@root' => $root])
      );
    }
    $form_state->setValue('root', $root);

    // Validate file pattern (structure).
    $structure = $form_state->getValue('structure');
    $structure = rtrim(ltrim($structure, static::PATH_TRIM), static::PATH_TRIM);
    $using_pattern = FALSE;
    if (empty($structure)) {
      // No structure specified, use a default file name.
      $entity_type = $this->externalEntityType
        ? ($this->externalEntityType->getDerivedEntityTypeId() ?? 'data') :
        'data';
      $structure =
        'xntt-'
        . preg_replace('#\W+#', '', $entity_type)
        . $this->fileExtension;
    }
    elseif (str_contains($structure, '{')) {
      $using_pattern = TRUE;
    }

    if ($using_pattern) {
      // Check if a pattern is used and is correct.
      $no_pattern_structure = preg_replace(
        '~\{' . static::SUBSTR_REGEXP . '\w+' . static::FIELDRE_REGEXP . '\}~',
        '',
        $structure
      );
      if (str_contains($no_pattern_structure, '{')) {
        $form_state->setErrorByName(
          'structure',
          $this->t(
            'The file name pattern "@structure" is not valid.',
            ['@structure' => $structure]
          )
        );
      }

      // Check filter pattern.
      $struct_re = $this->generateFileFilterRegex($structure);
      if (FALSE === preg_match($struct_re, '')) {
        $form_state->setErrorByName(
          'structure',
          $this->t(
            'At least one regular expression used in the file name pattern "@pattern" is not valid. Generated regex: "@pattern_re". Error message: @preg_message',
            [
              '@pattern' => $structure,
              '@pattern_re' => $struct_re,
              '@preg_message' => preg_last_error_msg(),
            ]
          )
        );
      }
      else {
        $this->fileFilterRegex = $struct_re;
      }
    }
    $form_state->setValue('structure', $structure);

    // File name pattern restriction.
    $matching_only = $form_state->getValue('matching_only') ?? FALSE;
    $form_state->setValue('matching_only', $matching_only);

    // Multiple records by file.
    $multi_records = $form_state->getValue('multi_records') ?? FALSE;
    $form_state->setValue('multi_records', $multi_records);

    // Index file.
    $use_index = $form_state->getValue('use_index');
    $form_state->setValue('use_index', $use_index);

    $index_file = $form_state->getValue('index_file');
    $index_file = rtrim(
      ltrim($index_file, static::PATH_TRIM),
      static::PATH_TRIM
    );
    if (!empty($index_file)) {
      // Check file scheme.
      if (0 === preg_match('#^\w+://#', $index_file)) {
        $index_file = $default_scheme . $index_file;
      }
      if (!file_exists($index_file)) {
        // Tries to create the file.
        if (touch($index_file)) {
          $this->messenger->addMessage(
            'The selected index file "'
            . $index_file
            . '" did not exist and was created.'
          );
        }
        $this->logger->info('Created missing index file "' . $index_file . '".');
      }
      if (!file_exists($index_file) || !is_file($index_file)) {
        $form_state->setErrorByName(
          'index_file',
          $this->t(
            'File not accessible "@index_file".',
            ['@index_file' => $index_file]
          )
        );
      }
    }
    $form_state->setValue('index_file', $index_file);

    // Field list.
    $field_list = trim($form_state->getValue('field_list'));
    $has_id_field = (preg_match(
      '#(?:^|[ ,;])\Q' . $this->getIdField() . '\E(?:[ ,;]|$)#',
      $field_list
    ));
    if (!empty($field_list) && !$has_id_field) {
      $form_state->setErrorByName(
        'field_list',
        $this->t('Field list must contain the entity identifier.')
      );
    }
    $form_state->setValue('field_list', $field_list);

    $this->setConfiguration($form_state->getValues());
  }

  /**
   * Submission handler for regenerate index file button.
   */
  public function regenerateIndexFileSubmit(
    array &$form,
    FormStateInterface $form_state
  ) {
    $this->launchIndexFileRegenerationBatch(
      $form_state,
      'regenerate',
      'Regenerating entity-file index file.'
    );
  }

  /**
   * Submission handler for update index file button.
   */
  public function updateIndexFileSubmit(
    array &$form,
    FormStateInterface $form_state
  ) {
    $this->launchIndexFileRegenerationBatch(
      $form_state,
      'update',
      'Updating entity-file index file... Loading current index.'
    );
  }

  /**
   * Launch batch jobs to regenerate/update index file.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $mode
   *   The update mode. One of 'regenerate' or 'update'.
   * @param string $title
   *   Batch title.
   */
  public function launchIndexFileRegenerationBatch(
    FormStateInterface $form_state,
    string $mode,
    string $title
  ) {
    // Stay on current form after processing.
    $form_state->disableRedirect();

    // Get index file.
    $xntt_type = $this->externalEntityType->getDerivedEntityTypeId();
    $plugin_id = $this->getPluginId();
    $root = $this->configuration['root'];
    $structure = $this->configuration['structure'];
    $index_file = $this->configuration['index_file'];
    $regex = $this->generateFileFilterRegex();
    $params = [
      'xntt_type'  => $xntt_type,
      'plugin'     => $plugin_id,
      'root'       => $root,
      'structure'  => $structure,
      'index_file' => $index_file,
      'regex'      => $regex,
    ];
    $operations = [
      [
        'xnttfiles_regenerate_index_file_process',
        [
          // Params.
          $params,
          $mode,
        ],
      ],
    ];
    $batch = [
      'title' => $title,
      'operations' => $operations,
      'finished' => 'xnttfiles_regenerate_index_file_finished',
      'progressive' => TRUE,
    ];
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(ExternalEntityInterface $entity) {
    $entity_data = $entity->toRawData();
    // Get entity file path.
    if (empty($entity_file_path = $this->getEntityFilePath($entity_data))) {
      // Not found.
      return;
    }

    // Single file?
    if (!$this->configuration['multi_records']) {
      // Remove file.
      if ($success = unlink($entity_file_path)) {
        unset($this->fileEntityData[$entity_file_path][$entity_id]);
      }
    }
    else {
      // Get entities in file.
      $entities = &$this->getFileEntityData($entity_file_path);
      if (!empty($entities)) {
        $entity_id = $entity_data[$this->getIdField()];
        $initial_count = count($entities);
        // Remove the requested entity.
        unset($entities[$entity_id]);
        // Check if we removed something.
        if (count($entities) == ($initial_count - 1)) {
          $success = TRUE;
          $this->saveFile($entity_file_path, $entities);
          if ($this->useIndexFile) {
            $this->removeEntityFileIndex($entity_id, TRUE);
          }
        }
      }
    }

    if (empty($success)) {
      $this->messenger->addError(
        'Failed to delete entity ' . $entity->id()
      );
      $this->logger->error(
        'Failed to delete entity '
        . $entity->id()
        . ' in file "'
        . $entity_file_path
        . '"'
      );
    }

    return SAVED_DELETED;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    $data = [];
    // Load each entity.
    if (!empty($ids) && is_array($ids)) {
      foreach ($ids as $id) {
        $data[$id] = $this->load($id);
      }
    }
    return $data;
  }

  /**
   * Loads one entity.
   *
   * @param mixed $id
   *   The ID of the entity to load.
   * @param array|null $values
   *   Member values to use as default.
   *
   * @return array
   *   A raw data array, empty array if no data found.
   */
  public function load($id, $values = NULL) {
    $entity_data = [$this->getIdField() => $id];
    if (!empty($values)) {
      $entity_data += $values;
    }
    // Get entity path.
    if (empty($entity_file_path = $this->getEntityFilePath($entity_data))) {
      // Not found.
      return [];
    }
    // Get entity data.
    $entity = $this->getFileEntityData($entity_file_path)[$id] ?? [];

    if (empty($entity)) {
      $entity = [];
    }
    else {
      // Add provided values if missing.
      $entity += $entity_data;
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(ExternalEntityInterface $entity) {
    $entity_data = $entity->toRawData();
    // Get entity path.
    if (empty($entity_file_path = $this->getEntityFilePath($entity_data))) {
      // No file, stop here.
      return;
    }
    // Get entities in file.
    $entities = &$this->getFileEntityData($entity_file_path);
    // Check if file was empty.
    if (empty($entities) || !array_key_exists('', $entities)) {
      // Add columns.
      if ($fields = $this->configuration['field_list']) {
        $entities[''] = preg_split('#[ ,;]+#', $fields);
        $this->logger->info(
          '"'
          . $entity_file_path
          . '" file did not exist and was created using configuration columns.'
        );
      }
      else {
        // Save all available fields.
        $entities[''] = array_keys($entity_data);
        $this->logger->info(
          '"'
          . $entity_file_path
          . '" file did not exist and was created using entity "'
          . $entity_data[$this->getIdField()]
          . '" columns ('
          . ($this->externalEntityType
            ? $this->externalEntityType->getDerivedEntityTypeId() . ' data type'
            : 'data type not set')
          . ').'
        );
      }
    }

    // Check if ID exists in base.
    $idf = $this->getIdField();
    if (isset($entity_data[$idf]) && ('' != $entity_data[$idf])) {
      // Update existing.
      $entity_id = $entity_data[$idf];
      $entities[$entity_id] = $entity_data;
      $this->saveFile($entity_file_path, $entities);
      $result = SAVED_UPDATED;
    }
    else {
      // Save new.
      // Create a new id.
      $all_entity_ids = array_keys($this->entityFileIndex + $entities);
      $entity_data[$idf] = max(array_keys($all_entity_ids)) ?: 0;
      ++$entity_data[$idf];
      $this->messenger->addWarning(
        $this->t(
          'A new external entity identifier "@id" had to be generated. Be aware that it may conflict with existing entities not stored in the same file and not registered in the entity-file index file.',
          ['@id' => $entity_data[$idf]]
        )
      );
      $this->logger->warning(
        'New external entity identifier generated for type "'
        . $this->externalEntityType->getDerivedEntityTypeId()
        . '": "'
        . $entity_data[$idf]
        . '"'
      );
      $entity_id = $entity_data[$idf];
      $entities[$entity_id] = $entity_data;
      $this->saveFile($entity_file_path, $entities);
      $result = SAVED_NEW;
    }
    // Recore file in index if needed.
    if (($this->useIndexFile) && (!$this->isEntityIndexed($entity_id))) {
      $this->appendEntityFileIndex($entity_id, $entity_file_path, TRUE);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function query(
    array $parameters = [],
    array $sorts = [],
    $start = NULL,
    $length = NULL,
    bool $reload = FALSE
  ) {
    static $cache;
    $cache_key = spl_object_hash($this);
    if (!empty($parameters)) {
      ksort($parameters);
      $cache_key .= http_build_query($parameters);
    }

    if (empty($cache[$cache_key]) || $reload) {
      // See ExternalEntityStorageClientInterface::query
      // Get list of files.
      $files = $this->getAllEntitiesFilePath($this->configuration['use_index']);
      sort($files);
      $matching_entities = [];
      foreach ($files as $file) {
        // Load file data.
        $entities = $this->loadFile($file);
        // Filter matching entities.
        if (!empty($parameters)) {
          $field_mapper = $this->externalEntityType->getFieldMapper();
          foreach ($entities as $id => $entity) {
            $match = TRUE;
            foreach ($parameters as $param) {
              if (!is_array($param)) {
                $this->logger->warning(
                  'Invalid filter parameter in filter query: ' . print_r($param, TRUE)
                );
                continue;
              }
              // Works with simple field mapper as it returns the field name.
              // @todo handle other field mappers that may return expressions.
              // @todo get the relevant property name rather than hardcoding 'value' (ex. 'uri' for links).
              $mapped_field =
                $field_mapper->getFieldMapping($param['field'])['value']
                ?? NULL;

              if (!isset($entity[$mapped_field])) {
                $match = FALSE;
                break 1;
              }
              switch ($param['operator']) {
                case '=':
                case '==':
                case '===':
                  if ($entity[$mapped_field] != $param['value']) {
                    $match = FALSE;
                    break 2;
                  }
                  break;

                case 'IN':
                  if (!(in_array($entity[$mapped_field], (array) $param['value']))) {
                    $match = FALSE;
                    break 2;
                  }
                  break;

                case '!=':
                case '!==':
                case '<>':
                  if ($entity[$mapped_field] == $param['value']) {
                    $match = FALSE;
                    break 2;
                  }
                  break;

                case 'NOT IN':
                  if (in_array($entity[$mapped_field], (array) $param['value'])) {
                    $match = FALSE;
                    break 2;
                  }
                  break;

                case '<':
                  if (!($entity[$mapped_field] < $param['value'])) {
                    $match = FALSE;
                    break 2;
                  }
                  break;

                case '<=':
                  if (!($entity[$mapped_field] <= $param['value'])) {
                    $match = FALSE;
                    break 2;
                  }
                  break;

                case '>':
                  if (!($entity[$mapped_field] > $param['value'])) {
                    $match = FALSE;
                    break 2;
                  }
                  break;

                case '>=':
                  if (!($entity[$mapped_field] >= $param['value'])) {
                    $match = FALSE;
                    break 2;
                  }
                  break;

                default:
                  $this->logger->warning(
                    'Unsupported operator "'
                    . $param['operator']
                    . '" in query filtering.'
                  );
                  $match = FALSE;
                  break 2;
              }
            }
            if ($match) {
              $matching_entities[] = $entity;
            }
          }
        }
        else {
          $matching_entities = array_merge($matching_entities, $entities);
        }
      }

      $cache[$cache_key] = $matching_entities;
    }

    $matching_entities = $cache[$cache_key];
    if ($sorts) {
      usort($matching_entities, function ($a, $b) use ($sorts) {
        $order = 0;
        foreach ($sorts as $sort) {
          if ($sort['direction'] == 'asc') {
            $order = $a[$sort['field']] <=> $b[$sort['field']];
          }
          elseif ($sort['direction'] == 'desc') {
            $order = $b[$sort['field']] <=> $a[$sort['field']];
          }
          else {
            // Warn for unknown direction.
            $this->messenger->addWarning(
              'Unkown sort direction "'
              . $sort['direction']
              . '" for field "'
              . $sort['field']
              . '".'
            );
          }
          if ($order) {
            break;
          }
        }
        return $order;
      });
    }

    if ($start) {
      $matching_entities = array_slice($matching_entities, $start, $length);
    }

    return $matching_entities;
  }

  /**
   * {@inheritdoc}
   */
  public function countQuery(array $parameters = []) {
    $count = count($this->query($parameters));
    return $count;
  }

  /**
   * Returns the field name used as identifier.
   *
   * @param bool $reset_cache
   *   TRUE to reset cached id field value.
   *
   * @return string
   *   The entity field name used as entitiy identifier.
   */
  public function getIdField($reset_cache = FALSE) :string {
    static $id_field_cache;
    if (empty($id_field_cache[spl_object_hash($this)]) || $reset_cache) {
      // If the xntt is new, no field mapper is available.
      if (!empty($xntt = $this->externalEntityType)
          && ($xntt instanceof ExternalEntityTypeInterface)
          && (!empty($xntt->getFieldMapperId()))
      ) {
        // Catch exception in case current field mapper has been removed.
        try {
          $field_mapper = $xntt->getFieldMapper();
          $field_mapping = $field_mapper->getFieldMapping('id');
          if (!empty($field_mapping['value'])) {
            $id_field_cache[spl_object_hash($this)] = $field_mapping['value'];
          }
        }
        catch (PluginNotFoundException $e) {
          // The external entity type has not been saved yet. Do nothing.
        }
      }
    }

    return $id_field_cache[spl_object_hash($this)] ?? 'id';
  }

  /**
   * Turns a file path into a valid entity identifier compatible with URLs.
   *
   * @param string $file_path
   *   The file path.
   *
   * @return string
   *   The identifier.
   */
  public function filePathToId(string $file_path) :string {
    return str_replace('/', '\\', $file_path);
  }

  /**
   * Turns a "file path" id into a regular file path.
   *
   * @param string $id
   *   The entity identifier that uses a file path as base.
   *
   * @return string
   *   The corresponding file path.
   */
  public function idToFilePath(string $id) :string {
    return str_replace('\\', '/', $id);
  }

  /**
   * Generates a regular expression from a file name pattern.
   *
   * @param string $structure
   *   A file name pattern as defined in this module documentation.
   *   If omitted, it will use the pattern provided by the configuration.
   *
   * @return string
   *   A regular expression that can match valid external entity files (
   *   excluding the data main directory part).
   */
  public function generateFileFilterRegex(?string $structure = NULL) :string {
    $struct_re = $structure ?? $this->configuration['structure'] ?? '';
    // First, replace placeholders with user-provided regex.
    $pattern_re =
      '~\{' . static::SUBSTR_REGEXP . '(\w+)' . static::FIELDRE_REGEXP . '\}~';
    $struct_re = preg_replace_callback(
      $pattern_re,
      function ($matches) {
        // Get filter pattern to use. If none specified, allow word characters
        // and dash.
        $re = $matches[4] ?? '[\w\-]+';
        // If using a substring, just use filter pattern.
        if ('' != $matches[1]) {
          return '\E' . $re . '\Q';
        }
        else {
          // Full field string, use named capture filter pattern.
          return '\E(?P<' . $matches[3] . '>' . $re . ')\Q';
        }
      },
      $struct_re
    );
    // Then remove duplicate captures.
    preg_match_all('#\?P<\w+>#', $struct_re, $matches);
    foreach (array_unique($matches[0]) as $named_capture) {
      $named_capture_pos = strpos($struct_re, $named_capture) + strlen($named_capture);
      $struct_re =
        substr($struct_re, 0, $named_capture_pos)
        . str_replace(
            $named_capture,
            '',
            substr($struct_re, $named_capture_pos)
        );
    }
    // Finish regex and cleanup useless '\Q\E'.
    $struct_re = preg_replace('#\\\\Q\\\\E#', '', '#^\Q' . $struct_re . '\E$#');

    return $struct_re;
  }

  /**
   * Extract values from file path using file name pattern.
   *
   * @param string $file_path
   *   The file path from wich field values could be extracted.
   *
   * @return array
   *   Array of field values keyed by field names.
   */
  public function getValuesFromPath(string $file_path) :array {
    $values = [];
    $path = $this->configuration['root'];
    $structure = $this->configuration['structure'];
    if (FALSE !== strpos($structure, '{')) {
      // Remove root from file path to match (if present).
      $file_path_to_match = preg_replace('#^\Q' . $path . '\E/?#', '', $file_path);
      if (FALSE === preg_match($this->fileFilterRegex, $file_path_to_match, $values)) {
        $this->messenger->addError(
          $this->t(
            'Failed to extract field values from given pattern. Please check external entity file name pattern. @preg_message',
            [
              '@preg_message' => preg_last_error_msg(),
            ]
          )
        );
        $this->logger->error(
          'Failed to extract field values in "'
          . $file_path
          . '" from given file name pattern "'
          . $structure
          . '" using generated regex "'
          . $struct_re
          . '": '
          . preg_last_error_msg()
        );
      }
      else {
        // Got values, remove duplicates keyed by integers.
        $values = array_filter(
          $values,
          function ($k) {
            return !is_int($k);
          },
          ARRAY_FILTER_USE_KEY
        );

        // Set a default id using file path with backslash if missing.
        if (empty($values[$this->getIdField()])) {
          $values[$this->getIdField()] =
            $this->filePathToId($file_path_to_match);
        }
      }
    }
    return $values;
  }

  /**
   * Loads entity file index from index file if available.
   *
   * @param bool $reload
   *   If TRUE, clear cache and reload data from index file.
   *
   * @return array
   *   An array which keys are entity identifiers and values are corresponding
   *   data file path relative the the data directory set in config.
   */
  public function getEntityFileIndex(bool $reload = FALSE) :array {
    if (empty($this->entityFileIndex) || $reload) {
      $entity_file_index = [];
      if (!empty($index_file = $this->configuration['index_file'])
          && !empty($this->externalEntityType)
          && ($this->externalEntityType instanceof ExternalEntityTypeInterface)
      ) {
        // Nothing from cache, check for an index file.
        if (!empty($raw_lines = file($index_file))) {
          $line_number = 1;
          foreach ($raw_lines as $raw_line) {
            $values = explode("\t", substr($raw_line, 0, -1));
            if (2 == count($values)) {
              // Make sure we are using a correct file scheme.
              if (0 === preg_match('#^\w+://#', $values[1])) {
                $values[1] =
                  $this->configuration['root']
                  . '/'
                  . $values[1];
              }
              $entity_file_index[$values[0]] = $values[1];
            }
            elseif (FALSE === preg_match('!^\s*(?:(?:/|;|#).*)$!', $raw_line)) {
              // Not a comment or an empty line, warn for a file format error.
              $this->logger->warning(
                'Invalid line format in index file "'
                . $index_file
                . '" at line '
                . $line_number
                . ': "'
                . $raw_line
                . '"'
              );
            }
            ++$line_number;
          }
        }
      }
      $this->entityFileIndex = $entity_file_index;
    }

    return $this->entityFileIndex;
  }

  /**
   * Set the entity file index to use.
   *
   * @param array $entity_file_index
   *   An array of entity file path relative to the data directory set in
   *   config, keyed by corresponding entity identifiers.
   * @param bool $save
   *   If TRUE, saves the update into the index file otherwise, the file is left
   *   unchanged and just current class instance will use the changes.
   */
  public function setEntityFileIndex(array $entity_file_index, $save = FALSE) {
    $this->entityFileIndex = $entity_file_index;
    if ($save && !empty($index_file = $this->configuration['index_file'])) {
      if ($fh = fopen($index_file, 'w')) {
        $index_data = array_reduce(
          array_keys($entity_file_index),
          function ($data, $id) use ($entity_file_index) {
            $file_path = preg_replace(
              '#^(?:\Q' . $this->configuration['root'] . '\E)?/*#',
              '',
              $entity_file_index[$id]
            );
            return $data . $id . "\t" . $file_path . "\n";
          },
          ''
        );
        $index_data = preg_replace('#^\s+#', '', $index_data);
        fwrite($fh, $index_data);
        fclose($fh);
      }
      else {
        $this->logger->warning(
          'Failed to save entity-file index file "'
          . $index_file
          . '".'
        );
      }
    }
  }

  /**
   * Tells if an entity is indexed.
   *
   * @param string $entity_id
   *   The identifier of the entity to check.
   *
   * @return bool
   *   TRUE if the entity is indexed, FALSE otherwise.
   */
  public function isEntityIndexed(string $entity_id) :bool {
    return ($entity_file_index = $this->getEntityFileIndex())
      && !empty($entity_file_index[$entity_id]);
  }

  /**
   * Removes an entity identifier from the entity-file index.
   *
   * @param string $entity_id
   *   Entity identifier to remove from the index file.
   * @param bool $save
   *   If TRUE, saves the update into the index file otherwise, the file is left
   *   unchanged and just current class instance will use the changes.
   */
  protected function removeEntityFileIndex(string $entity_id, $save = FALSE) {
    unset($this->entityFileIndex[$entity_id]);
    if ($save && !empty($index_file = $this->configuration['index_file'])) {
      $index_data = file_get_contents($index_file);
      $index_data = preg_replace(
        '#^\Q' . $entity_id . '\E\t[^\n]*(?:\n|$)#m',
        '',
        $index_data
      );
      if ($fh = fopen($index_file, 'w')) {
        fwrite($fh, $index_data);
        fclose($fh);
      }
      else {
        $this->logger->warning(
          'Failed to remove entity "'
          . $entity_id
          . '" from index file "'
          . $index_file
          . '".'
        );
      }
    }
  }

  /**
   * Append an entity identifier to the entity-file index file.
   *
   * @param string $entity_id
   *   Entity identifier to add to the index file.
   * @param string $path
   *   Corresponding entity file path.
   * @param bool $save
   *   If TRUE, saves the update into the index file otherwise, the file is left
   *   unchanged and just current class instance will use the changes.
   */
  protected function appendEntityFileIndex(
    string $entity_id,
    string $path,
    $save = FALSE
  ) {
    $file_path = preg_replace(
      '#^(?:\Q' . $this->configuration['root'] . '\E)?/*#',
      '',
      $path
    );
    if (0 === preg_match('#^\w+://#', $path)) {
      $path = $this->configuration['root'] . '/' . $path;
    }
    $this->entityFileIndex[$entity_id] = $path;
    if ($save && !empty($index_file = $this->configuration['index_file'])) {
      if ($fh = fopen($index_file, 'a+')) {
        // Check if last character is an EOL.
        $stat = fstat($fh);
        if ($stat['size'] > 1) {
          fseek($fh, $stat['size'] - 1);
          if ("\n" != fgetc($fh)) {
            // Nope, add one.
            fwrite($fh, "\n");
          }
        }
        fwrite($fh, $entity_id . "\t" . $file_path . "\n");
        fclose($fh);
      }
      else {
        $this->logger->warning(
          'Failed to add entity "'
          . $entity_id
          . '" to index file "'
          . $index_file
          . '".'
        );
      }
    }
  }

  /**
   * Returns all available external entity files matching this entity type.
   *
   * @param bool $only_from_index
   *   If TRUE, no directory listing will be done and only the provided file
   *   index will be used. Default: FALSE.
   *
   * @return array
   *   An array of file path (including file schemes).
   */
  public function getAllEntitiesFilePath(bool $only_from_index = FALSE)
    :array {
    $files_path = [];

    // Make sure indexed entity records are loaded first.
    if ($entity_file_index = $this->getEntityFileIndex()) {
      $files_path = array_unique(array_values($entity_file_index));
    }

    if (!$only_from_index) {
      $path = $this->configuration['root'];
      $structure = $this->configuration['structure'];
      if (empty($structure)) {
        $this->messenger->addError(
          'No file name or file name pattern specified in the config. Please check entity type settings.'
        );
        return [];
      }
      // Check if the user specified a pattern rather than a regular file.
      if (str_contains($structure, '{')) {
        // There is a pattern, get all available files matching the filter.
        $root_length = strlen($path) + 1;
        $scan_for_files = function ($dir)
 use (&$scan_for_files, $root_length) {
          $matching_files = [];
          $files = scandir($dir, SCANDIR_SORT_NONE);
          if ($files) {
            foreach ($files as $file) {
              $file_path = "$dir/$file";
              // Remove leading root path.
              $path_to_filter = substr($file_path, $root_length);
              if (is_file($file_path)
                  && (preg_match($this->fileFilterRegex, $path_to_filter))
              ) {
                $matching_files[$file_path] = TRUE;
              }
              elseif (is_dir($file_path) && !str_starts_with($file, '.')) {
                $matching_files += $scan_for_files($file_path);
              }
            }
          }
          return $matching_files;
        };
        // Get file path from directory search.
        $files_path = array_unique(
          array_merge(
            array_keys($scan_for_files($path)),
            $files_path
          )
        );
      }
      else {
        // Regular (single) file.
        $files_path[] = $path . '/' . $structure;
        $files_path = array_unique($files_path);
      }
    }

    return $files_path;
  }

  /**
   * Returns the file path of the file containing to the given entity.
   *
   * @param array|object $entity
   *   The entity of wich the path is requested.
   *
   * @return string
   *   Returns the path to the file file corresponding to the given entity
   *   including file scheme or an empty string if not available.
   */
  public function getEntityFilePath($entity) :string {
    // Make sure we work on an array of values.
    if (is_object($entity)) {
      $entity = $entity->toRawData();
    }

    // Get data root directory.
    $root = $path = $this->configuration['root'];
    $id = $entity[$this->getIdField()];

    // Check if we got an indexed file for this entity.
    if ($entity_file_index = $this->getEntityFileIndex()) {
      $entity_path = $entity_file_index[$id] ?? NULL;
      if (!empty($entity_path)) {
        // We got a specific file path for this entity, stop here.
        if (0 === preg_match('#^\w+://#', $entity_path)) {
          $entity_path = $this->configuration['root'] . '/' . $entity_path;
        }
        return $entity_path;
      }
    }

    if ($this->configuration['use_index']) {
      // Failed to find entity file from the index.
      $this->messenger->addWarning(
        $this->t(
          'Entity "@id" not found in the index file. Please check your "entity-file index" file.',
          ['@id' => $id]
        )
      );
      $this->logger->warning(
        'Entity "'
        . $id
        . '" not found in the index file. Please check your "entity-file index" file.'
      );
      return '';
    }

    // Get file name pattern.
    $structure = $this->configuration['structure'];
    if (empty($structure)) {
      $this->messenger->addError(
        $this->t(
          'No file name or file name pattern specified in the config. Please check entity type settings.'
        )
      );
      $this->logger->error(
        'No file name or file name pattern specified in the config. Please check entity type settings.'
      );
      return '';
    }
    // Check if the user specified a pattern rather than a regular file.
    if (str_contains($structure, '{')) {
      // Replace pattern placeholders by their field values.
      if (FALSE === preg_match_all(
            '~\{' . static::SUBSTR_REGEXP . '(\w+)' . static::FIELDRE_REGEXP . '\}~',
            $structure,
            $matches,
            PREG_SET_ORDER
          )
      ) {
        // We got a problem.
        $this->messenger->addError(
          $this->t(
            'Invalid sub-directory pattern.'
          )
        );
        $this->logger->error(
          'Invalid sub-directory pattern. Message: ' . preg_last_error_msg()
        );
        return '';
      }
      $subdir = $structure;
      $unmatched_fields = [];
      foreach ($matches as $match) {
        $offset = $match[1];
        $length = $match[2];
        $field  = $match[3];
        // Check if we are missing an entity field value.
        if (!array_key_exists($field, $entity)) {
          $unmatched_fields[] = $field;
          break;
        }
        if ('' == $offset) {
          $offset = 0;
        }
        // Process substrings.
        if (empty($length)) {
          $pat_replacement = substr($entity[$field], $offset);
        }
        else {
          $pat_replacement = substr($entity[$field], $offset, $length);
        }
        $subdir = str_replace($match[0], $pat_replacement, $subdir);
      }
      // Check if we must use indexation because a field was missing.
      if (!empty($unmatched_fields)) {
        // If the identifier is not part of the file name pattern and it has not
        // been provided in the index file, it means the identifier is the file
        // path.
        if (!str_contains($structure, '{' . $this->getIdField())
            && ($file_path = $this->idToFilePath($id))
        ) {
          $path .= '/' . $file_path;
        }
        else {
          // File indexation did not index this entity. We can't guess the file.
          if (!empty($index_file = $this->configuration['index_file'])) {
            $this->logger->warning(
              'Entity "'
              . $id
              . '" not present in index file ('
              . $index_file
              . ') while file name pattern uses unavailable fields: '
              . implode(', ', $unmatched_fields)
            );
            $this->messenger->addWarning(
              $this->t(
                'Entity "@id" not present in index file while file name pattern uses unavailable fields.',
                ['@id' => $id]
              )
            );
            return '';
          }
          else {
            // Unmatched fields in patterns and identifier was used.
            $this->logger->warning(
              'No index file set while using non-identifier fields in pattern: '
              . implode(', ', $unmatched_fields)
            );
            $this->messenger->addWarning(
              $this->t(
                'No index file set while using non-identifier fields in pattern.'
              )
            );
            return '';
          }
        }
      }
      else {
        // All patterns matched.
        $path .= '/' . $subdir;
      }

      // Make sure the file path matches the file name pattern.
      if ($this->configuration['matching_only']) {
        $path_to_check = substr($path, strlen($root) + 1);
        if (!preg_match($this->fileFilterRegex, $path_to_check)) {
          $this->logger->warning(
            'The file path "'
            . $path
            . '" corresponding to the given entity '
            . $id
            . ' does not match the entity type file name pattern while file name pattern has been enforced in entity type configuration.'
          );
          $this->messenger->addWarning(
            $this->t(
              'Entity file path does not match the entity type file name pattern.'
            )
          );
          return '';
        }
      }
    }
    else {
      // Just a single main file to use.
      $path .= '/' . $structure;
    }

    return $path;
  }

  /**
   * Loads the given file or use cached data and return loaded entities.
   *
   * @param string $file_path
   *   Full path to an existing file.
   * @param bool $with_column_names
   *   If TRUE, columns names will be included in the returned array using the
   *   key '' (empty string).
   * @param bool $reload
   *   If TRUE, reloads the cache if it exists.
   *
   * @return array
   *   Returns the loaded array of entities keyed by identifiers.
   */
  public function loadFile(
    string $file_path,
    bool $with_column_names = FALSE,
    bool $reload = FALSE
  ) :array {
    $entities = $this->getFileEntityData($file_path, $reload);
    if (!$with_column_names) {
      unset($entities['']);
    }
    return $entities;
  }

  /**
   * Load the given file or return corresponding class member data if available.
   *
   * @param string $file_path
   *   Full path to an existing file.
   * @param bool $reload
   *   If TRUE, reloads the cache if it exists.
   *
   * @return array
   *   Returns a reference to the corresponding class member containing the
   *   loaded entities keyed by identifiers.
   */
  protected function &getFileEntityData(string $file_path, bool $reload = FALSE)
  :array {
    if (!file_exists($file_path)) {
      // Log issue only once (by thread).
      static $logged_warnings = [];
      if (empty($logged_warnings[$file_path])) {
        $this->logger->warning(
          'Unable to load data: file "' . $file_path . '" does not exist.'
        );
        $logged_warnings[$file_path] = TRUE;
      }
      $this->fileEntityData[$file_path] = [];
    }
    elseif (empty($this->fileEntityData[$file_path]) || $reload) {
      $this->fileEntityData[$file_path] = $this->parseFile($file_path);
    }
    return $this->fileEntityData[$file_path];
  }

  /**
   * Parse file content according to the given format.
   *
   * The returned array must contain entities (array) keyed by identifier. An
   * additional special key, the empty string '', must be filled with an array
   * of entity field names.
   *
   * @param string $file_path
   *   Full path to an existing file.
   *
   * @return array
   *   An array of loaded entities keyed by identifiers. There is also the
   *   special key empty string '' used to store the array of column (field)
   *   names.
   */
  abstract protected function parseFile(string $file_path) :array;

  /**
   * Saves the given file or return cached data if available.
   *
   * Client implementations may override this in order to remove empty files.
   *
   * @param string $file_path
   *   Full path to an existing file.
   * @param array $entities_data
   *   Array of entities keyed by identifiers plus the column (field) names
   *   keyed using the empty string key ''.
   *
   * @throw \Drupal\xnttfiles\Exception\FilesExternalEntityException
   */
  public function saveFile(string $file_path, array $entities_data) {
    // Make sure we got something valid to store.
    if (empty($entities_data[''])) {
      throw new FilesExternalEntityException(
        'Invalid entity data to save in file: no field names set.'
      );
    }

    // Check if directory exist.
    $directory = dirname($file_path);
    if (($directory != '.') && (!file_exists($directory))) {
      // Try to create directory structure.
      if (FALSE === mkdir($directory, 0777, TRUE)) {
        throw new FilesExternalEntityException(
          'Unable to create the specified directory where the file should be stored: '
          . $directory
        );
      }
    }

    // Prepare header and footer.
    $file_header = $this->generateFileHeader($file_path, $entities_data);
    $file_footer = $this->generateFileFooter($file_path, $entities_data);

    $fh = fopen($file_path, 'w');
    if (!$fh) {
      throw new FilesExternalEntityException(
        'Unable to write the specified file: '
        . $file_path
      );
    }
    elseif (!file_exists($file_path)) {
      throw new FilesExternalEntityException(
        'Unaccessible or inexisting file: '
        . $file_path
      );
    }

    // @todo use a safer approach to avoid issue if file writting fails:
    // - Create and fill a new file.
    // - Rename old file.
    // - Rename new file.
    // - Remove old file.
    $entities_raw_data = $this->generateRawData($entities_data);

    if ('' !== $file_header) {
      fwrite($fh, $file_header);
    }
    fwrite($fh, $entities_raw_data);
    if ('' !== $file_footer) {
      fwrite($fh, $file_footer);
    }

    // Update cache data member.
    $this->fileEntityData[$file_path] = $entities_data;
    fclose($fh);
  }

  /**
   * Generates a raw string of entity data ready to be written in a file.
   *
   * @param array $entities_data
   *   An array of entity data.
   *
   * @return string
   *   The raw string that can be written into an entity data file.
   */
  abstract protected function generateRawData(array $entities_data) :string;

  /**
   * Generates file header.
   *
   * @param string $file_path
   *   Path to the original file.
   * @param array $entities_data
   *   An array of entity data.
   *
   * @return string
   *   The file header.
   */
  protected function generateFileHeader(
    string $file_path,
    array $entities_data
  ) :string {
    return '';
  }

  /**
   * Generates file footer.
   *
   * @param string $file_path
   *   Path to the original file.
   * @param array $entities_data
   *   An array of entity data.
   *
   * @return string
   *   The file header.
   */
  protected function generateFileFooter(
    string $file_path,
    array $entities_data
  ) :string {
    return '';
  }

}
