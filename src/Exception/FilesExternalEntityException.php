<?php

namespace Drupal\xnttfiles\Exception;

/**
 * Exception thrown by External Entities Files Storage Client.
 */
class FilesExternalEntityException extends \RuntimeException {}
